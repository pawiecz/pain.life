#-------------------------------------------------
#
# Project created by QtCreator 2014-05-20T20:08:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Life0
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    leditem.cpp \
    matrix.cpp

HEADERS  += mainwindow.h \
    leditem.h \
    matrix.h

FORMS    += mainwindow.ui

OTHER_FILES +=

RESOURCES += \
    led.qrc
