#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "leditem.h"
#include "matrix.h"

#include <QGraphicsScene>
#include <QMainWindow>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setCell(char color);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    Matrix *matrix;
    QTimer *timer;

private slots:
    void startClicked();
    void stopClicked();
    void loadClicked();
    void saveClicked();
    void emptyClicked();
    void randomClicked();
    void drawBlueClicked();
    void drawRedClicked();
    void sliderTouched(int value);
};

#endif // MAINWINDOW_H
