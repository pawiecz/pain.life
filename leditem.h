#ifndef LEDITEM_H
#define LEDITEM_H

#include <QDebug>
#include <QPainter>
#include <QGraphicsItem>

class LedItem : public QGraphicsObject
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity NOTIFY opacityChanged)
public:
    QImage red;
    QImage blue;
    QImage off;

    QPointF pos;

    LedItem(QPointF pos);

    QRectF boundingRect() const;

    // overriding paint()
    void paint(QPainter * painter,
               const QStyleOptionGraphicsItem * option,
               QWidget * widget);

    // item state
    bool state;
    char cell[2];

protected:

signals:
    qreal opacityChanged(qreal opacity);
public slots:
    void advance(int phase);
};

#endif // LEDITEM_H
