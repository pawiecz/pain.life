#include "leditem.h"

#include <QGraphicsItemAnimation>
#include <QPropertyAnimation>
#include <qtimeline.h>

LedItem::LedItem(QPointF _pos) : pos(_pos)
{
    red.load(":/led/red.png");
    blue.load(":/led/blue.png");
    off.load(":/led/white.png");

    state = false;
    cell[0] = 'd';
    cell[1] = 'd';
}

QRectF LedItem::boundingRect() const
{
    return QRect(pos.x(), pos.y(),16,16);
}

void LedItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget)
    Q_UNUSED(option)
    switch(cell[state])
    {
    case 'd':
        painter->drawImage(pos,off); break;
    case 'r':
        painter->drawImage(pos,red); break;
    case 'b':
        painter->drawImage(pos,blue); break;
    }
}

void LedItem::advance(int phase)
{
    if(!phase) return;
    QPropertyAnimation *animation = new QPropertyAnimation(this, "opacity");
    animation->setDuration(1000);
    animation->setEasingCurve(QEasingCurve::Linear);
    animation->setStartValue(1.0);
    animation->setKeyValueAt(0.5, 0.0);
    animation->setEndValue(1.0);

    animation->start();
}
