#ifndef MATRIX_H
#define MATRIX_H

#include <QObject>
#include <QPainter>
#include <QGraphicsWidget>
#include <QtWidgets>
#include "leditem.h"
#include <vector>

#define SIZEX 50
#define SIZEY 30

class Matrix : public QGraphicsItem
{

public:
    Matrix();

    int itab[8];
    int jtab[8];
    bool flag;
    int red;
    int blue;
    static char cell;

    std::vector<std::vector<LedItem*> > matrix;

    QRectF boundingRect() const;

    // overriding paint()
    void paint(QPainter * painter,
               const QStyleOptionGraphicsItem * option,
               QWidget * widget);

    bool modified;
    bool scribbling;
    QPointF lastPoint;

    void readData(QString fileName);

public slots:
    void advance(int phase);
    void emptyMatrix();
    void randomMatrix();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
};

#endif // MATRIX_H
