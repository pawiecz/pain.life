#include "matrix.h"

char Matrix::cell = 'b';

Matrix::Matrix()
{
    for(int i = 0; i < SIZEX; ++i){
        std::vector<LedItem*> line;
        for(int j = 0; j < SIZEY; ++j){
            QPointF pos(16*i, 16*j);
            LedItem *item = new LedItem(pos);
            item->setParentItem(this);
            line.push_back(item);
        }
        matrix.push_back(line);
    }

    flag = false;
    modified = false;
    scribbling = false;

    itab[0]=-1; itab[1]=-1; itab[2]=-1; itab[3]=0;
    itab[4]=1; itab[5]=1; itab[6]=1; itab[7]=0;

    jtab[0]=-1; jtab[1]=0; jtab[2]=1; jtab[3]=1;
    jtab[4]=1; jtab[5]=0; jtab[6]=-1; jtab[7]=-1;

}

QRectF Matrix::boundingRect() const
{
    return QRect(0,0,SIZEX*16,SIZEY*16);
}

void Matrix::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter)
    Q_UNUSED(widget)
    Q_UNUSED(option)
    QRectF rect = boundingRect();
    update(rect);
}

void Matrix::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        matrix[(int)event->pos().x()/16][(int)event->pos().y()/16]->cell[flag]=cell;
        lastPoint = event->pos();
        scribbling = true;
    }
}

void Matrix::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if ((event->buttons() & Qt::LeftButton) && scribbling)
        matrix[(int)event->pos().x()/16][(int)event->pos().y()/16]->cell[flag]=cell;
}

void Matrix::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && scribbling) {
        matrix[(int)event->pos().x()/16][(int)event->pos().y()/16]->cell[flag]=cell;
        scribbling = false;
    }
}


void Matrix::advance(int phase)
{
    if(!phase) return;

    int i,j;
    for(i = 1; i < SIZEX-1; ++i){
        for(j=1; j<SIZEY-1; ++j){
            red = 0;
            blue = 0;
            for(int k=0; k!=8; ++k)
            {
                red += 'r'==matrix[i+itab[k]][j+jtab[k]]->cell[flag]?1:0;
                blue += 'b'==matrix[i+itab[k]][j+jtab[k]]->cell[flag]?1:0;
            }

            if(matrix[i][j]->cell[flag] == 'd')
            {
                if(red+blue == 3)
                    matrix[i][j]->cell[!flag] = (red>=blue)?'r':'b';
                else
                    matrix[i][j]->cell[!flag] = 'd';
            }
            else
            {
                if((red+blue !=2 )&&(red+blue != 3))
                    matrix[i][j]->cell[!flag] = 'd';
                else
                    matrix[i][j]->cell[!flag] = matrix[i][j]->cell[flag];

            }
            matrix[i][j]->state=!matrix[i][j]->state;
        }
    }
    flag=!flag;
}

void Matrix::emptyMatrix()
{
    for(int i = 0; i < SIZEX; ++i)
    {
        for(int j = 0; j < SIZEY; ++j)
        {
            matrix[i][j]->cell[0] = 'd';
            matrix[i][j]->cell[1] = 'd';
        }
    }
}

void Matrix::randomMatrix()
{
    for(int i = 1; i < SIZEX-1; ++i)
    {
        for(int j = 1; j < SIZEY-1; ++j)
        {
            switch(qrand() % 8)
            {
            case 1:
                matrix[i][j]->cell[0] = 'r';
                matrix[i][j]->cell[1] = 'r'; break;
            case 2:
                matrix[i][j]->cell[0] = 'b';
                matrix[i][j]->cell[1] = 'b'; break;
            default:
                matrix[i][j]->cell[0] = 'd';
                matrix[i][j]->cell[1] = 'd'; break;
            }
        }
    }
}

void Matrix::readData(QString fileName)
{
    QString line;
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return;
    QTextStream in(&file);

    flag = 0;
    for(int i = 0; i < SIZEY; ++i)
    {
        line = in.readLine();
        for(int j = 0; j < SIZEX; ++j)
        {
            if('R' == line[j])
            {
                matrix[j][i]->cell[0] = 'r';
                matrix[j][i]->cell[1] = 'r';
            }
            else if('B' == line[j])
            {
                matrix[j][i]->cell[0] = 'b';
                matrix[j][i]->cell[1] = 'b';
            }
            else
            {
                matrix[j][i]->cell[0] = 'd';
                matrix[j][i]->cell[1] = 'd';
            }
        }
    }
}
