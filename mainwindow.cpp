#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setRenderHint(QPainter::SmoothPixmapTransform);
ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    matrix = new Matrix();
    scene->addItem(matrix);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), scene, SLOT(advance()));
    connect(ui->actionStart, SIGNAL(triggered(bool)), this, SLOT(startClicked()));
    connect(ui->actionStop, SIGNAL(triggered(bool)), this, SLOT(stopClicked()));
    connect(ui->actionOpen, SIGNAL(triggered(bool)), this, SLOT(loadClicked()));
    connect(ui->actionSave, SIGNAL(triggered(bool)), this, SLOT(saveClicked()));
    connect(ui->actionNew_empty, SIGNAL(triggered(bool)), this, SLOT(emptyClicked()));
    connect(ui->actionNew_random, SIGNAL(triggered(bool)), this, SLOT(randomClicked()));
    connect(ui->actionDraw_blue_cells, SIGNAL(triggered(bool)), this, SLOT(drawBlueClicked()));
    connect(ui->actionDraw_red_cells, SIGNAL(triggered(bool)), this, SLOT(drawRedClicked()));
    connect(ui->verticalSlider, SIGNAL(valueChanged(int)), this, SLOT(sliderTouched(int)));

    ui->actionStop->setEnabled(false);
    setWindowTitle("QGlider 0.2");
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event)
    ui->graphicsView->fitInView(matrix, Qt::KeepAspectRatio);
}

void MainWindow::startClicked()
{
    timer->start(ui->verticalSlider->value());

    ui->actionNew_empty->setEnabled(false);
    ui->actionNew_random->setEnabled(false);
    ui->actionOpen->setEnabled(false);
    ui->actionSave->setEnabled(false);
    ui->actionStart->setEnabled(false);
    ui->actionStop->setEnabled(true);
    ui->actionDraw_blue_cells->setEnabled(false);
    ui->actionDraw_red_cells->setEnabled(false);
}

void MainWindow::stopClicked()
{
    timer->stop();

    ui->actionNew_empty->setEnabled(true);
    ui->actionNew_random->setEnabled(true);
    ui->actionOpen->setEnabled(true);
    ui->actionSave->setEnabled(true);
    ui->actionStart->setEnabled(true);
    ui->actionStop->setEnabled(false);
    ui->actionDraw_blue_cells->setEnabled(true);
    ui->actionDraw_red_cells->setEnabled(true);
}

void MainWindow::loadClicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
         tr("Open file"), "",
         tr("QGlider file (*.gld)"));
    matrix->readData(fileName);
}

void MainWindow::emptyClicked()
{
    matrix->emptyMatrix();
}

void MainWindow::randomClicked()
{
    matrix->randomMatrix();
}

void MainWindow::drawBlueClicked()
{
    setCell('b');
    ui->actionDraw_blue_cells->setChecked(true);
    ui->actionDraw_red_cells->setChecked(false);
}

void MainWindow::drawRedClicked()
{
    setCell('r');
    ui->actionDraw_blue_cells->setChecked(false);
    ui->actionDraw_red_cells->setChecked(true);
}

void MainWindow::sliderTouched(int value)
{
    if(timer->isActive())
    {
        timer->stop();
        timer->start(value);
    }
}

void MainWindow::setCell(char color)
{
    matrix->cell = color;
}

void MainWindow::saveClicked()
{

}
